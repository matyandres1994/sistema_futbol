<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <title>
            </title>
        </meta>
        <link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" rel="stylesheet">
            <script src="../js/jquery-3.3.1.js">
            </script>
            <script src="../js/bootstrap.min.js">
            </script>
            <link href="../toast/toastr.min.css" rel="stylesheet" type="text/css">
                <script src="../toast/toastr.min.js" type="text/javascript">
                </script>
                <script src="../js/paginaEquipo.js">
                </script>
            </link>
        </link>
    </head>
    <body>
        <?php include 'nav.php'?>
        <div class="container" id="main">
            <div id="contieneTabla" style="background-color: #000000;filter:alpha(opacity=50); opacity:0.8;color: #00FF21">
            </div>

                <div class="modal fade " id="modalJugadores" role="dialog" tabindex="-1" ">
        <div class="modal-dialog modal-xl" role="document" >
            <div class="modal-content" style="filter:alpha(opacity=50); opacity:0.8;">
                <div class="modal-header">
                    <h5 class="modal-title">
                        Listado de jugadores
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body"">
                    <div id="tablaJugadores"  style="background-color: #000000;filter:alpha(opacity=50); opacity:0.8;color: #00FF21">

                    </div>
                </div>
                <div class="modal-footer">

                     <button class="btn btn-outline-secondary" data-dismiss="modal" type="button">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
        </div>
         <script crossorigin="anonymous" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js">
    </script>
    </body>
</html>
