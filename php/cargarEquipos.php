<?php
include 'conexion.php';

$respuesta     = "";
$resultadohtml = "";
session_start();
$rut = $_SESSION['rut'];

$query = "select e.IdEquipo,e.NombreEquipo from equipos e,aprueba a where e.Idequipo=a.IdEquipo AND a.RutJugador='$rut'";
$datos = mysqli_query($conn, $query);
$resultadohtml .= "<table id='tablaproductos' class='table table-striped'>";
$resultadohtml .= "<tr><td>Codigo</td><td>Nombre</td><td>Ver Jugadores</td><td>Añadir Jugador</td>";
while ($fila = mysqli_fetch_array($datos)) {
    $resultadohtml .= "<tr data-id=" . $fila["IdEquipo"] . "><td>" . $fila["IdEquipo"] . "</td>";
    $resultadohtml .= "<td>" . $fila["NombreEquipo"] . "</td>";
    $resultadohtml .= " <td><a class='btnVer btn btn-outline-primary'>Ver Jugadores</a></td>";
    $resultadohtml .= " <td><a class='btnAñadir btn btn-outline-success' href='registrarCompañeros.php?id=" . $fila["IdEquipo"] . "'>Añadir Jugador</a></td>";

}
$resultadohtml .= "</table>";

$respuesta = "ok";

mysqli_close($conn);

echo json_encode(array("respuesta" => $respuesta, "resultadohtml" => $resultadohtml));
