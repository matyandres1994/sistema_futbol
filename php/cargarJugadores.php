<?php
include 'conexion.php';

$respuesta     = "";
$resultadohtml = "";
session_start();
$rut = $_SESSION['rut'];

$query = "select p.IdPartido,p.FechaPartido,p.HoraPartido,u.Region,u.Cuidad,u.Direccion  from Partidos p,ubicacion u where p.IdUbicacion=u.IdUbicacion ";
$datos = mysqli_query($conn, $query);
$resultadohtml .= "<table id='tablaproductos' class='table table-striped'>";
$resultadohtml .= "<tr><td>Codigo</td><td>Fecha</td><td>Hora</td><td>Region</td><td>Cuidad</td><td>Direccion</td>";
while ($fila = mysqli_fetch_array($datos)) {
    $resultadohtml .= "<tr data-id=" . $fila["IdPartido"] . "><td>" . $fila["IdPartido"] . "</td>";
    $resultadohtml .= "<td>" . $fila["FechaPartido"] . "</td>";
    $resultadohtml .= "<td>" . $fila["HoraPartido"] . "</td>";
    $resultadohtml .= "<td>" . $fila["Region"] . "</td>";
    $resultadohtml .= "<td>" . $fila["Cuidad"] . "</td>";
    $resultadohtml .= "<td>" . $fila["Direccion"] . "</td>";

}
$resultadohtml .= "</table>";

$respuesta = "ok";

mysqli_close($conn);

echo json_encode(array("respuesta" => $respuesta, "resultadohtml" => $resultadohtml));
