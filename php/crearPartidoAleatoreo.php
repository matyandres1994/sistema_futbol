<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <title>
                Crear partido
            </title>
        </meta>
        <style type="text/css">
            h4{
                color: #000000;
                font-size: 20px;
            }
        </style>
    </head>
    <body>
        <?php include 'nav.php'?>
        <div class="container" id="main" style="background-color: #000000; filter:alpha(opacity=50); opacity:0.9; margin-top: 20px;">

            <h1 style="text-align: center;">
                Crear partido
            </h1>
            <form action="IngresarPartidoAleatorio.php" class="" method="POST" style="margin-top: 20px;">
                <h4>
                    Fecha del partido :
                </h4>
                <input autocomplete="off" class="form-control" id="fechaPartido" name="fechaPartido" required=""  type="date">
                </input>
                <h4>
                    Hora del partido :
                </h4>
                <input autocomplete="off" class="form-control" id="horaPartido" name="horaPartido" required=""  type="time">
                </input>
                <h4>
                    Cantidad de jugadores :
                </h4>
                <select class="form-control"  required="" id="numeroJugadores" name="numeroJugadores">
                    <option disabled="" selected="">
                        Seleccione
                    </option>
                    <option value="12">
                        12 - 6v6
                    </option>
                    <option value="12">
                        10 - 5v5
                    </option>
                </select>


                            <h4>
                            Seleccionar Region :
                            </h4>


                <select class="form-control" id="Region" name="Region"  required="">
                            <option disabled="" selected="">Seleccione</option>
                            <option value="Bio-Bio">Bio-Bio</option>
                        </select>
                        <h4>
                            Seleccionar Cuidad :
                            </h4>


                <select class="form-control" id="Cuidad" name="Cuidad" required="">
                            <option disabled="" selected="">Seleccione</option>
                            <option value="Concepcion">Concepcion</option>ç
                            <option value="Coronel">Coronel</option>
                        </select>
                        <h4>
                    Ingrese direccion :
                </h4>

                <input autocomplete="off" class="form-control" id="Direccion" name="Direccion" required=""  type="text" placeholder="Pasaje-poblacion-Numero">
                </input>
                <input type="submit" name="" value="Crear" class="btn btn-dark  form-control" style="margin-top: 20px;">
            </form>
        </div>
    </body>
</html>
