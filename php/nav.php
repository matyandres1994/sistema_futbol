<?php
session_start();
if ($_SESSION['logeado'] != 'si') {
    header("location: ../index.php?sesion=true");
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <title>
                Organizador de partidos
            </title>
        </meta>
        <link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" rel="stylesheet">
        </link>
        <script src="../js/jquery-3.3.1.js">
        </script>
        <script src="../js/bootstrap.min.js">
        </script>
        <link href="../toast/toastr.min.css" rel="stylesheet" type="text/css">
            <script src="../toast/toastr.min.js" type="text/javascript">
            </script>
            <link href="toast/toastr.min.css" rel="stylesheet">
            </link>
            <script src="toast/toastr.min.js">
            </script>
            <script>
                function openNav() {
    document.getElementById("sideNavigation").style.width = "250px";

}

function closeNav() {
    document.getElementById("sideNavigation").style.width = "0";

}
            </script>
            <style type="text/css">
                /* The side navigation menu */
.sidenav {
    height: 100%; /* 100% Full-height */
    width: 0; /* 0 width - change this with JavaScript */
    position: fixed; /* Stay in place */
    z-index: 1; /* Stay on top */
    top: 0;
    left: 0;
    background-color: #111; /* Black*/
    overflow-x: hidden; /* Disable horizontal scroll */
    padding-top: 60px; /* Place content 60px from the top */
    transition: 0.5s; /* 0.5 second transition effect to slide in the sidenav */
}

/* The navigation menu links */
.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 20px;
    color: #818181;
    display: block;
    transition: 0.3s

}

/* When you mouse over the navigation links, change their color */
.sidenav a:hover, .offcanvas a:focus{
    color: #f1f1f1;
}

/* Position and style the close button (top right corner) */
.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 30px;
    margin-left: 50px;
}

/* Style page content - use this if you want to push the page content to the right when you open the side navigation */
#main {
    transition: margin-left .5s;
    padding: 20px;
    overflow:hidden;
    width:100%;

    height: 100%;

}
body {
  overflow-x: hidden;
  background-image: url("../img/fondo.jpg");
}

/* Add a black background color to the top navigation */
.topnav {
    background-color:#303030;
    overflow: hidden;
}

/* Style the links inside the navigation bar */
.topnav a {
    float: left;
    display: block;
    color:#FFFFFF;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    font-size: 10px;

}

/* Change the color of links on hover */
.topnav a:hover {

    color: white;
}

/* Add a color to the active/current link */
.topnav a.active {
    background-color: #4CAF50;
    color: white;
}

/* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}

a svg{
  transition:all .5s ease;

  &:hover{
    #transform:rotate(180deg);
  }
}

#ico{
  display: none;
}

.menu{
  background: #000;
  display: none;
  padding: 5px;
  width: 320px;
  @include border-radius(5px);

  #transition: all 0.5s ease;

  a{
    display: block;
    color: #fff;#CANCEL
    text-align: center;
    padding: 10px 2px;
    margin: 3px 0;
    text-decoration: none;
    background: #444;


    &:nth-child(1){
      margin-top: 0;
      @include border-radius(3px 3px 0 0 );
    }
    &:nth-child(5){
      margin-bottom: 0;
      @include border-radius(0 0 3px 3px);
    }

    &:hover{
      background: #555;
    }
  }
}
            </style>
        </link>
    </head>
</html>
<body>
    <header>
        <div class="sidenav" id="sideNavigation">
            <a class="closebtn" href="javascript:void(0)" onclick="closeNav()">
                x
            </a>
            <a href="crearPartido.php">
                Crear partido Equipo/Equipo
            </a>
            <a href="crearPartidoAleatoreo.php">
                Crear partido aleatorio
            </a>
            <a href="verPartidoDisponible.php">
                Ver partidos disponible
            </a>
            <a href="crearEquipo.php">
                Crear equipo
            </a>
            <a href="verEquipos.php">
                Ver mis equipos
            </a>

        </div>
        <nav class="topnav">
            <a href="#" onclick="openNav()">
                <svg height="30" id="icoOpen" width="30">
                    <path d="M0,5 30,5" stroke="#000" stroke-width="5">
                    </path>
                    <path d="M0,14 30,14" stroke="#000" stroke-width="5">
                    </path>
                    <path d="M0,23 30,23" stroke="#000" stroke-width="5">
                    </path>
                </svg>
            </a>
            <a class="btn" href="../index.php" style="color: #FFFFFF; font-size: 15px; text-align: center;margin-top: 2px;margin-left:1300px;">Cerrar sesion</a>
        </nav>
    </header>
</body>
