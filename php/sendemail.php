<?php
require 'PHPMailer / PHPMailerAutoload.php';

$mail = nuevoPHPMailer;

$mail->isSMTP(); // Configurar el correo para usar SMTP
$mail->Host                     = 'smtp.gmail.com'; // Especifique los servidores SMTP principales y de respaldo
$mail->SMTPAuth                 = true; // Habilitar la autenticación SMTP
$mail->Nombredeusuario          = 'Dirección de correo electrónico'; // Nombre
deusuarioSMTP$mail->Contraseña = 'Contraseña de cuenta de correo electrónico'; // contraseña SMTP
$mail->SMTPSecure               = 'tls'; // Habilitar el cifrado TLS, `ssl` también aceptó
$mail->Port                     = 587; // Puerto TCP para conectarse a

$mail->setFrom('info@example.com', 'CodexWorld');
$mail->addReplyTo('info@example.com', 'CodexWorld');
$mail->addAddress('john @ gmail.);   // Agregar un destinatario ');
$mail->addCC('cc@example.com');
$mail->addBCC('bcc@example.com');

$mail->isHTML(true); // Establezca el formato del correo electrónico en HTML

$bodyContent    = '<h1> Cómo enviar un correo electrónico usando PHP en Localhost por CodexWorld </h1>';
$bodyContent .  = '<p> Este es el correo electrónico HTML enviado desde localhost usando un script PHP por <b>CodexWorld</b> </p>';

$mail->Subject = 'Correo electrónico de Localhost por CodexWorld';
$mail->Body    = $bodyContent;

si(!$mail->send()) {
    echo 'No se pudo enviar el mensaje.';
    echo 'Error del remitente:' . $mail->ErrorInfo;
} else {
    echo 'El mensaje ha sido enviado';
}
