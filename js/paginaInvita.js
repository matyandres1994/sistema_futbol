$(document).ready(function() {
    cargarTabla();

    function cargarTabla() {
        var codigo = $('#idequipo').val();
        $.ajax({
            type: "GET",
            url: "../php/cargarJugadoresxEquipo1.php",
            data: {
                'codigo': codigo
            },
            success: function(response) {
                var datos1 = JSON.parse(response);
                if (datos1.respuesta != "ok") {
                    alert();
                } else {
                    $("#tablaJugadores").html("");
                    $("#tablaJugadores").html(datos1.resultadohtml);
                    $(".btnInvitar").click(function() {
                        var codigo = $(this).parents("tr").find("td")[0].innerHTML;
                        var nombre = $(this).parents("tr").find("td")[1].innerHTML;
                        var Email = $(this).parents("tr").find("td")[3].innerHTML;
                        $.ajax({
                            type: "GET",
                            url: "../php/emailEnviar.php",
                            data: {
                                'codigo': codigo,
                                'nombre': nombre,
                                'email': Email
                            },
                            success: function(response) {
                                toastr.success("Se a enviado un correo al jugador");
                            },
                            failure: function(response) {}
                        });
                    });
                }
            },
            failure: function(response) {
                alert();
            }
        });
    }
});