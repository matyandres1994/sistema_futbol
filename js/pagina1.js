$(document).ready(function() {
    function cargarTabla() {
        var codigo = $('#email1').val();
        var idequipo = $('#idequipo').val();
        if (codigo.length <= 11) {
            alert("Ingrese el correo del jugador a buscar");
        } else {
            $.ajax({
                type: "GET",
                url: "../php/cargarUsuarios.php",
                data: {
                    'codigo': codigo,
                    'idequipo': idequipo
                },
                success: function(response) {
                    var datos = JSON.parse(response);
                    if (datos.respuesta != "ok") {
                        alert();
                    } else {
                        $("#contieneTabla").html("");
                        $("#contieneTabla").html(datos.resultadohtml);
                        $("#agregar").click(function() {
                            var idequipo = $('#idequipo').val();
                            var codigo = $(this).parents("tr").find("td")[0].innerHTML;
                            $.ajax({
                                type: "GET",
                                url: "../php/registrarCompañeros1.php",
                                data: {
                                    'codigo': codigo,
                                    'id': idequipo
                                },
                                success: function(response) {
                                    $("#contieneTabla").html("");
                                    toastr.success("Se a añadido al equipo");
                                },
                                failure: function(response) {}
                            });
                        });
                    }
                },
                failure: function(response) {
                    alert();
                }
            });
        }
    }
    $("#buscar").click(function() {
        cargarTabla();
    });
    $("#agregar").click(function() {
        alert();
    });

    function eliminar() {
        var codigo = $('#prueba1').val();
        $.ajax({
            type: "GET",
            url: "~/../DAL/eliminar.php",
            data: {
                'codigo': codigo
            },
            datatype: "application/json",
            success: function(response) {
                toastr.success("Registro eliminado");
                cargarTabla();
                $('#ModalEliminar').modal('hide');
            },
            failure: function(response) {
                alert(response);
            }
        });
    }

    function Modificar() {
        var codigo = $('#Codigoedit').val();
        var nombre = $('#descripcionedit').val();
        $.ajax({
            type: "GET",
            url: "~/../DAL/modificarprod.php",
            data: {
                'codigo': codigo,
                'nombre': nombre
            },
            datatype: "application/json",
            success: function(response) {
                toastr.success("Registro Modificado");
                cargarTabla();
                $('#modalEdita').modal('hide');
            },
            failure: function(response) {
                alert(response);
            }
        });
    }
});