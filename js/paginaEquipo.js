$(document).ready(function() {
    cargarTabla();

    function cargarTabla() {
        $.ajax({
            type: "GET",
            url: "../php/cargarEquipos.php",
            success: function(response) {
                var datos = JSON.parse(response);
                if (datos.respuesta != "ok") {
                    alert();
                } else {
                    $("#contieneTabla").html("");
                    $("#contieneTabla").html(datos.resultadohtml);
                    $(".btnVer").click(function() {
                        $("#modalJugadores").modal("show");
                        var codigo = $(this).parents("tr").find("td")[0].innerHTML;
                        $.ajax({
                            type: "GET",
                            url: "../php/cargarJugadoresxEquipo.php",
                            data: {
                                'codigo': codigo
                            },
                            success: function(response) {
                                var datos1 = JSON.parse(response);
                                if (datos.respuesta != "ok") {
                                    alert();
                                } else {
                                    $("#tablaJugadores").html("");
                                    $("#tablaJugadores").html(datos1.resultadohtml);
                                }
                            },
                            failure: function(response) {}
                        });
                    });
                }
            },
            failure: function(response) {
                alert();
            }
        });
    }
});