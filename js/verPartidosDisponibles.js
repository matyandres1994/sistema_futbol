$(document).ready(function() {
    cargarTabla();

    function cargarTabla() {
        $.ajax({
            type: "GET",
            url: "../php/cargarJugadores.php",
            success: function(response) {
                var datos = JSON.parse(response);
                if (datos.respuesta != "ok") {
                    alert();
                } else {
                    $("#contieneTabla").html("");
                    $("#contieneTabla").html(datos.resultadohtml);
                }
            },
            failure: function(response) {
                alert();
            }
        });
    }
});