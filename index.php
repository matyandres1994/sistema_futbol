<?php
session_start();
session_destroy();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
            <title>
                Login
            </title>
            <link crossorigin="anonymous" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" rel="stylesheet">
                <script src="js/jquery-3.3.1.js">
                </script>
                <script src="js/bootstrap.min.js">
                </script>
                <link href="toast/toastr.min.css" rel="stylesheet" type="text/css">
                    <script src="toast/toastr.min.js" type="text/javascript">
                    </script>
                    <script src="js/index.js">
                    </script>
                    <style type="text/css">
                        body{
            background-image: url('img/login.jpg');
            background-repeat: no-repeat;
background-attachment: fixed

        }
        #login{

            margin-top: 10%;
            height: 400px;

            color: #FFFFFF;


        }
        .inputMIO{

            background-color: #555BA9;
            border-top: none;
            border-left: none;
            border-right:  none;

            outline: none;


            width: 100%;
            border-bottom-width: 1px;
            border-bottom-color: #464848;
            font-size: 20px;

        }
        label{
            color: #267F00;
            font-size: 25px;
        }
                    </style>
                </link>
            </link>
        </meta>
    </head>

<body>
    <div class="offset-3 col-6" id="login" style="filter:alpha(opacity=50); opacity:0.8;">
        <h2 style="margin-top: 10px; text-align: center;">
            Bienvenido
        </h2>
        <form action="php/verificarUsuario.php" method="POST" style="margin-top: 40px;">
            <div class="form-group row">
                <label class="offset-2 col-lg-2" for="">
                    Correo:
                </label>
                <div class="col-lg-6">
                    <input autocomplete="off" class="inputMIO" id="Correo" name="Correo" required="" style="margin-left: 20px;" type="email" style="filter:alpha(opacity=50); opacity:0.8;">
                    </input>
                </div>
            </div>
            <div class="form-group row">
                <label class="offset-2 col-lg-2" for="">
                    Password:
                </label>
                <div class="col-lg-6">
                    <input autocomplete="off" class="inputMIO" id="password" name="password" required="" style="margin-left: 20px;" type="password">
                    </input>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-4 col-lg-6">
                    <input class="form-control btn-secondary" id="btnForm" name="btnForm" required="" style="margin-left: 20px;" type="submit">
                    </input>
                </div>
            </div>


        </form>
 <div class="form-group row">
                <div class="offset-4 col-lg-6">
                 <button class="form-control btn btn-dark " data-target="#modalJugador" data-toggle="modal" style="margin-top: 20px; margin-left: 20px;">
            Registrar
        </button>
          </div>
        </div>

        <?php
if (isset($_GET["fallo"]) && $_GET["fallo"] == 'true') {
    ?>
        <script type="text/javascript">
            toastr.error('Datos incorrectos');
        </script>
<?php
}
if (isset($_GET["sesion"]) && $_GET["sesion"] == 'true') {
    ?>
        <script type="text/javascript">
            toastr.error('No se puede acceder a la plataforma sin identificarte');
        </script>
        <?php
}

?>
    </div>
    <div class="modal fade " id="modalJugador" role="dialog" tabindex="-1" ">
        <div class="modal-dialog modal-xl" role="document" >
            <div class="modal-content" style="filter:alpha(opacity=50); opacity:0.8;">
                <div class="modal-header">
                    <h5 class="modal-title">
                        Registrar jugador
                    </h5>
                    <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">
                            ×
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        Rut jugador:
                        <input class="form-control" id="Codigoedit" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        Nombre jugador:
                        <input class="form-control" id="descripcionedit" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        Email jugador:
                        <input class="form-control" id="EmailJugador" type="email">
                        </input>
                    </div>
                    <div class="form-group">
                        Password:
                        <input class="form-control" id="passwordJugador" type="password">
                        </input>
                    </div>
                    <div class="form-group">
                        Repita password:
                        <input class="form-control" id="password1Jugador" type="password">
                        </input>
                    </div>
                    <div class="form-group">
                        Celular:
                        <input class="form-control" id="Celular" type="text">
                        </input>
                    </div>
                    <div class="form-group">
                        Posicion:
                        <select class="form-control" id="posicion">
                            <option disabled="" selected="">Seleccione</option>
                    <?php
include 'php/conexion.php';
$query = "select * from posiciones";
$datos = mysqli_query($conn, $query);
while ($fila = mysqli_fetch_array($datos)) {
    echo "<option value=" . $fila["IdPosicion"] . ">" . $fila["DescripcionPosicion"] . "</option>";
}
?>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-outline-primary" id="btnGuardar" type="button">
                        Guardar
                    </button>
                    <button class="btn btn-outline-secondary" data-dismiss="modal" type="button">
                        Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>
    <script crossorigin="anonymous" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js">
    </script>
</body>
</html>